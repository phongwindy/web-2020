var menuResponsive = {
    init: function() {
        this.menuToggle();
        this.toggleDropdown();
    },
    menuToggle: function() {
        var button = document.querySelector('.header__menu-bar');
        console.log(button);
        var menu = document.querySelector('.header__menu');
        button.addEventListener('click', function() {
            menu.classList.toggle('show');
        })
    },
    toggleDropdown: function() {
        var button = document.querySelector('.dropdown');
        var dropdown = document.querySelector('.header__menu-dropdown');
        button.addEventListener('click', function() {
            dropdown.classList.toggle('show')
        })
    }
}
menuResponsive.init();