$(document).ready(function() {
    $('.about__video a').fancybox();
});


var didScroll;
var lastScrollTop = 0;
var delta = 50;
var navbarHeight = $('.header').outerHeight();
$(window).scroll(function(event) {
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight) {
        // Scroll Down
        $('.header').css('top', '-150px');
    } else {
        // Scroll Up
        if (st + $(window).height() < $(document).height()) {
            $('.header').css('top', '0');
            $('.header').css('backgroundColor', 'white');
            $('.header__logo').css('color', 'black');
            $('.header__menu-item').css('color', 'black');
        }
    }
    lastScrollTop = st;
    if (lastScrollTop < 90) {
        $('.header').css('background', 'none');
        $('.header__logo').css('color', 'white');
        $('.header__menu-item').css('color', 'white');
    }
}

new WOW().init();

var menuResponsive = {
    init: function() {
        this.menuToggle();
        this.toggleDropdown();
    },
    menuToggle: function() {
        var button = document.querySelector('.header__item-menu-button');
        console.log(button);
        var menu = document.querySelector('.header__menu');
        button.addEventListener('click', function() {
            menu.classList.toggle('show');
        })
    },
    toggleDropdown: function() {
        var button = document.querySelector('.dropdown');
        var dropdown = document.querySelector('.header__menu-dropdown');
        button.addEventListener('click', function() {
            dropdown.classList.toggle('show')
        })
    }
}
menuResponsive.init();