 function functionScroll() {
     var section = document.querySelectorAll(".section"),
         sections = {},
         i = 0;

     Array.prototype.forEach.call(section, function(e) {
         sections[e.id] = e.offsetTop;
     });

     for (i in sections) {
         if (window.scrollY > 500) {
             document.querySelector('.header__item').style.height = '70px';
             if (sections[i] <= window.pageYOffset + 100) {
                 if (document.querySelector('.active') !== null) {
                     document.querySelector('.active').classList.remove('active');
                 }
                 document.querySelector('a[href*=' + i + ']').classList.add('active');
             }
         } else {
             document.querySelector('.header__item').style.height = '100px';
             document.querySelector('a[href*=' + i + ']').classList.remove('active');
         }
     }
 }

 window.addEventListener('scroll', functionScroll);
 window.addEventListener('resize', functionScroll);



 var menuResponsive = {
     init: function() {
         this.toggleMenu()
     },
     toggleMenu: function() {
         var button = document.querySelector('.menu-link');
         var menu = document.querySelector('.header__item_menu');
         button.addEventListener('click', function() {
             menu.classList.toggle('show')
         })
     }
 }
 menuResponsive.init()



 var modalBtns = Array.from(document.querySelectorAll(".portfolio__item_group"));
 modalBtns.forEach(function(btn) {
     btn.onclick = function() {
         var modal = btn.getAttribute('data-modals');
         document.getElementById(modal).style.display = "block";
     }
 });

 window.onclick = function(event) {
     if (event.target.className === "modal") {
         event.target.style.display = "none";
     }
 }

 var closeBtns = Array.from(document.querySelectorAll(".closes"));
 closeBtns.forEach(function(btn) {
     btn.onclick = function() {
         var modal = btn.closest('.modal');
         modal.style.display = "none";
     }
 });
 var closeBtns = Array.from(document.querySelectorAll(".closes-top"));
 closeBtns.forEach(function(btn) {
     btn.onclick = function() {
         var modal = btn.closest('.modal');
         modal.style.display = "none";
     }
 });