// $(document).ready(function() {
//     $('[data-fancy-box="image"]').fancybox({
//         loop: true
//     });
// });

//=================== Menu Scroll =================//
function functionScroll() {
    var section = document.querySelectorAll(".section"),
        sections = {},
        i = 0;

    Array.prototype.forEach.call(section, function(e) {
        sections[e.id] = e.offsetTop;
    });

    for (i in sections) {
        if (window.scrollY > 600) {
            if (sections[i] <= window.pageYOffset + 100) {
                if (document.querySelector('.active') !== null) {
                    document.querySelector('.active').classList.remove('active');
                }
                document.querySelector('a[href*=' + i + ']').classList.add('active');
            }
        } else {
            document.querySelector('a[href*=' + i + ']').classList.remove('active');
        }
    }
}

window.addEventListener('scroll', functionScroll);
window.addEventListener('resize', functionScroll);

function scrollFunction() {
    if (document.body.scrollTop > 40 || document.documentElement.scrollTop < 40) {
        document.querySelector(".header").style.background = 'none';
        document.querySelector(".header__logo").style.color = '#fde0d8';
        Array.from(document.querySelectorAll(".header__menu-list-link")).forEach(function(item) {
            item.classList.remove('black');
            item.classList.add('orange');
        })
    } else {
        document.querySelector(".header").style.backgroundColor = '#fff';
        document.querySelector(".header").style.index = "10000";
        document.querySelector(".header__logo").style.color = 'black';
        Array.from(document.querySelectorAll(".header__menu-list-link")).forEach(function(item) {
            item.classList.remove('orange');
            item.classList.add('black');
        })
    }
}


//========================== Menu Responsive ==========================//
var menuResponsive = {
    init: function() {
        this.toggleMenu()
    },
    toggleMenu: function() {
        var button = document.querySelector('.header__menu-button');
        var menu = document.querySelector('.header__menu');
        button.addEventListener('click', function() {
            menu.classList.toggle('show');
        })
    }
}
menuResponsive.init()
window.onscroll = function() { scrollFunction() };