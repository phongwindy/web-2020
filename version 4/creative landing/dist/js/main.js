$(document).ready(function() {
    $('.modal a').fancybox();
    $('.modal').owlCarousel({
            center: true,
            loop: true,
            margin: 10,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 2.5
                },
                1000: {
                    items: 4.7
                }
            }
        })
        //=================== adv carousel ==================//
    $('.adv__item').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

});

//=================== Scroll Menu ==================//

var didScroll;
var lastScrollTop = 0;
var delta = 50;
var navbarHeight = $('.header').outerHeight();
$(window).scroll(function(event) {
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight) {
        // Scroll Down
        $('.header').css('top', '-150px');
    } else {
        // Scroll Up
        if (st + $(window).height() < $(document).height()) {
            $('.header').css('top', '0');
            $('.header').css('backgroundColor', 'white');
            $('.header__menu-item a').css('color', 'black');
            $('.header__top').css('color', 'black');
            $('.header__logo').css('color', 'black');
        }
    }
    lastScrollTop = st;
    if (lastScrollTop < 50) {
        $('.header').css('background', 'none');
        $('.header').css('transition', '0.5s');
        $('.header a').css('color', 'white');
        $('.header__top').css('color', 'white');
        $('.header__logo').css('color', 'white');
    }
}
//=================== Menu Responsive ==================//


var menuResponsive = {
    init: function() {
        this.menuToggle();
    },
    menuToggle: function() {
        var button = document.querySelector('.header__menu-item-button');
        var menu = document.querySelector('.header__menu-item');
        button.addEventListener('click', function() {
            menu.classList.toggle('show');
        })
    }
}
menuResponsive.init();