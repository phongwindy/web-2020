//================ Scroll Menu ====================//
function functionScroll() {
    var section = document.querySelectorAll(".section"),
        sections = {},
        i = 0;

    Array.prototype.forEach.call(section, function(e) {
        sections[e.id] = e.offsetTop;
    });

    for (i in sections) {
        if (window.scrollY > 500) {
            document.querySelector('.header__item').style.height = '70px';
            if (sections[i] <= window.pageYOffset + 100) {
                if (document.querySelector('.active') !== null) {
                    document.querySelector('.active').classList.remove('active');
                }
                document.querySelector('a[href*=' + i + ']').classList.add('active');
            }
        } else {
            document.querySelector('.header__item').style.height = '100px';
            document.querySelector('a[href*=' + i + ']').classList.remove('active');
        }
    }
}

window.addEventListener('scroll', functionScroll);
window.addEventListener('resize', functionScroll);


//================== Menu Responsive =====================//
var menuResponsive = {
    init: function() {
        this.toggleMenu()
    },
    toggleMenu: function() {
        var button = document.querySelector('.menu-link');
        var menu = document.querySelector('.header__item_menu');
        button.addEventListener('click', function() {
            menu.classList.toggle('show')
        })
    }
}
menuResponsive.init()